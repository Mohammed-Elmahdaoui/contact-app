<?php

use App\Http\Controllers\ActivityController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\ContactNoteController;
use App\Http\Controllers\ContactsAdminController;
use App\Http\Controllers\ContactsController;
use App\Http\Controllers\TagsController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\TrushCompanyController;
use App\Http\Controllers\TrushContactsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/contacts', function () {
//    $contacts = getContacts();
//    $companies = getCompanies();
//    return view('contact.index', [
//        "contacts"=>$contacts,
//        "companies"=>$companies
//    ]);
//})->name("contacts.index");
//
//Route::get('/contacts/create', function () {
//    return view('contact.create');
//})->name("contacts.create");
//
//Route::get('/contacts/{id} ', function ($id) {
//    $contact = Contact::findOrFail($id);
//    return view('contact.show', ["contact"=>$contact]);
//})->name("contacts.show");


//Route::get('/contacts', [ContactsController::class, 'index'])->name("contacts.index");
//
//Route::get('/contacts/create', [ContactsController::class, 'create'])->name("contacts.create");
//
//Route::get('/contacts/{id} ', [ContactsController::class, 'show'])->name("contacts.show");


//
//Route::group([
//    'as'=>"contacts.",
//    'prefix'=>"/contacts",
//], function () {
//    Route::get('/', [ContactsController::class, 'index'])->name("index");
//    //        C 3 > Q 2
//    Route::post('/', [ContactsController::class, 'store'])->name("store");
//    Route::get('/create', [ContactsController::class, 'create'])->name("create");
//    Route::get('/{id} ', [ContactsController::class, 'show'])->name("show");
//});

Route::resource('/companies', CompanyController::class);
Route::resource('/contacts', ContactsController::class);


Route::group([
    'as' => "trush.",
    'prefix' => "/trush",
], function () {
    Route::resource('/contacts', TrushContactsController::class)
        ->only(['index', 'update', 'destroy']);
    Route::resource('/companies', TrushCompanyController::class)
        ->only(['index', 'update', 'destroy']);
});


Route::resources([
    'tag' => TagsController::class,
    'task' => TaskController::class
]);

Route::resource('/activity', ActivityController::class)
    ->only(['create', 'store', 'edit', 'update', 'destroy']);
Route::resource('/activity', ActivityController::class)
    ->except(['index', 'show']);

Route::resource('contact.note', ContactNoteController::class)
    ->shallow();

//Route::get('/ContactAdmin', [ContactsAdminController::class, 'index'])
//    ->middleware('check-user');

//Route::get('/ContactAdmin', [ContactsAdminController::class, 'index'])
//    ->middleware(['check-user', 'check-user-actif']);

Route::get('/ContactAdmin', [ContactsAdminController::class, 'index'])
    ->middleware('check');
