<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Contact;
use Illuminate\Http\Request;

class TrushContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
//        $contacts = Contact::withTrashed()->get();

        $contacts = Contact::onlyTrashed();
        if ($request->filled('company_id')) {
            $contacts->where('company_id', $request->company_id);
        }
        $contacts = $contacts->get();

        $companies = Company::all();
        return view('contacts.trush_contacts', [
            "contacts" => $contacts,
            "companies" => $companies
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update($contact)
    {
        $contact = Contact::withTrashed()->findOrFail($contact);
        $contact->restore();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($contact)
    {
        $contact = Contact::withTrashed()->findOrFail($contact);
        $contact->forceDelete();
        return redirect()->back();
    }

}
