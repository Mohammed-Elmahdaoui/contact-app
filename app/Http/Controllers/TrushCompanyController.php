<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Contact;
use Illuminate\Http\Request;

class TrushCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $companies = Company::onlyTrashed()->get();
        return view('companies.trush_companies', [
            "companies" => $companies
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update($company)
    {
        $company = Company::withTrashed()->findOrFail($company);
        $company->restore();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($company)
    {
        $company = Company::withTrashed()->findOrFail($company);
        $company->forceDelete();
        return redirect()->back();
    }

}
