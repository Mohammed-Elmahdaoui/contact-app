<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreContactRequests;
use App\Http\Requests\UpdateContactRequests;
use App\Models\Company;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
//        C6 Q3 a
//        $contacts = Contact::all();
//        C6 Q3 b
//        $contacts_first_3 = Contact::take(3)->get();
//        C6 Q3 c
//        $contacts_first_3_after_5 = Contact::skip(5)->take(3)->get();

//        C6 Q11
        $contacts = Contact::query();
        if ($request->filled('company_id')) {
            $contacts->where('company_id', $request->company_id);
        }
        $contacts = $contacts->get();


        $companies = Company::all();
        return view('contacts.index', [
            "contacts" => $contacts,
            "companies" => $companies
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $companies = Company::all();
        return view('contacts.create', [
            "companies" => $companies
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreContactRequests $request)
    {
//        C6 Q3 d
//        $contact = new Contact();
//        $contact->first_name = $request->first_name;
//        $contact->last_name = $request->last_name;
//        $contact->phone = $request->phone;
//        $contact->email = $request->email;
//        $contact->address = $request->address;
//        $contact->company_id = $request->company_id;
//        $contact->save();

//        C6 Q3 e
        Contact::create($request->all());

//        C6 Q3 f
        Contact::firstOrCreate(
            ['email' => $request->email],
            $request->all()
        );

        return redirect()->route('contacts.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Contact $contact)
    {
        return view('contacts.show', ["contact" => $contact]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Contact $contact)
    {
        $companies = Company::all();
        return view('contacts.edit', ["contact" => $contact, "companies" => $companies]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateContactRequests $request, Contact $contact)
    {

//        C6 Q3 g
//        $contact->first_name = $request->first_name;
//        $contact->last_name = $request->last_name;
//        $contact->phone = $request->phone;
//        $contact->email = $request->email;
//        $contact->address = $request->address;
//        $contact->company_id = $request->company_id;
//        $contact->save();

//        C6 Q3 h
        $contact->update($request->all());
        return redirect()->route('contacts.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Contact $contact)
    {
//        C6 Q3 i
        $contact->delete();
        return redirect()->route('contacts.index');
    }
}
