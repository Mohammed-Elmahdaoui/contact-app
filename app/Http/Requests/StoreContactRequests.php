<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreContactRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'first_name' => ['required', 'string', 'max:50'],
            'last_name' => ['required', 'string', 'max:50'],
            'phone' => ['nullable','regex:/^\+212[56][0-9]{8}$/'],
            'email' => ['required', 'email'],
            'address' => ['nullable', 'string'],
            'company_id' => ['required'],
        ];
    }

    public function attributes()
    {
        return [
            'first_name' => 'prenom',
            'last_name' => 'nom',
            'email' => 'adresse email',
            'phone' => 'numéro de téléphone',
            'company_id' => 'compagnie',
        ];
    }

    public function messages()
    {
        return [
            "first_name.required" => "Le prénom est obligatoire.",
            "first_name.string" => "Le prénom doit être une chaîne de caractères.",
            "first_name.max" => "Le prénom ne doit pas dépasser :max caractères.",
            "last_name.required" => "Le nom est obligatoire.",
            "last_name.string" => "Le nom doit être une chaîne de caractères.",
            "last_name.max" => "Le nom ne doit pas dépasser :max caractères.",
            "email.required" => "L'email est obligatoire.",
            "email.email" => "L'email doit être valide.",
            "company_id.required" => "Le champ company est obligatoire.",
        ];
    }
}
