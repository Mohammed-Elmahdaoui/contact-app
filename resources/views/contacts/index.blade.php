@extends('layouts.main')

@section("content")
    <div>
        <div class="d-flex justify-content-between bg-secondary p-3 rounded">
            <h2 class="text-light">All Contacts</h2>
            <a href="{{ route('contacts.create') }}" class="btn btn-light">Add New</a>
        </div>
        <div class="my-3">
            <form method="get" action="{{ route('contacts.index') }}">
                @csrf
                <div class="d-flex justify-content-end">
                    @includeUnless(empty($companies), 'contacts._company-selection')
                    @include('contacts._filter')
                </div>
            </form>
        </div>
        @if($contacts->count() > 0)
            <table class="table table-striped">
                <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Company</th>
                    <th>Actions</th>
                </tr>
                @foreach($contacts as $contact)
                    <tr>
                        <td>{{$contact->id}}</td>
                        <td>{{$contact->first_name}}</td>
                        <td>{{$contact->last_name}}</td>
                        <td>{{$contact->phone}}</td>
                        <td>{{$contact->email}}</td>
                        <td>{{$contact->company_id}}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{{ route('contacts.show', ['contact'=> $contact->id]) }}"
                                   class="btn btn-primary">Show</a>
                                <a href="{{ route('contacts.edit', ['contact'=> $contact->id]) }}"
                                   class="btn btn-warning">Edit</a>
                                <form action="{{ route('contacts.destroy', $contact->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        @else
            <div class="alert alert-info">No contacts found.</div>
        @endif
    </div>
@endsection
