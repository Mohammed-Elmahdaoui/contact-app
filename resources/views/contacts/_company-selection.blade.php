<select id="company_id" name="company_id" class="custom-select">
    <option value="">Select a company</option>
    @foreach($companies as $companie)
        <option value="{{ $companie->id }}">{{ $companie->name }}</option>
    @endforeach
</select>
