@extends('layouts.main')

@section("content")
    <div class="container">
        <div class="card rounded">
            <div class="card-header bg-secondary text-light">
                <h2>Contact:</h2>
            </div>
            <div class="card-body p-3">
                <table class="table table-borderless">
                    <tr>
                        <th>First Name</th>
                        <td>{{ $contact->first_name }}</td>
                    </tr>
                    <tr>
                        <th>Last Name</th>
                        <td>{{ $contact->last_name }}</td>
                    </tr>
                    <tr>
                        <th>Phone</th>
                        <td>{{ $contact->phone }}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{ $contact->email }}</td>
                    </tr>
                    <tr>
                        <th>Company</th>
                        <td>{{ $contact->company_id }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection
