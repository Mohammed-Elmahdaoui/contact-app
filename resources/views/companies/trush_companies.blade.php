@extends('layouts.main')

@section("content")
    <div>
        <div class="d-flex justify-content-between bg-secondary p-3 rounded">
            <h2 class="text-light">Deleted Companies</h2>
        </div>
        @if($companies->count() > 0)
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($companies as $company)
                    <tr>
                        <td>{{$company->id}}</td>
                        <td>{{$company->name}}</td>
                        <td>{{$company->address}}</td>
                        <td>{{$company->email}}</td>
                        <td>
                            <div class="btn-group">
                                <form action="{{ route('trush.companies.update', $company->id) }}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <button type="submit" class="btn btn-primary">Restore</button>
                                </form>
                                <form action="{{ route('trush.companies.destroy', $company->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Delete definitely</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="alert alert-info">No companies found.</div>
        @endif
    </div>
@endsection
