@extends('layouts.main')

@section("content")
    <div class="container">
        <div class="card rounded">
            <div class="card-header bg-secondary text-light">
                <h2>Company:</h2>
            </div>
            <div class="card-body p-3">
                <table class="table table-borderless">
                    <tr>
                        <th style="width: 150px;">Name</th>
                        <td>{{ $company->name }}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{ $company->email }}</td>
                    </tr>
                    <tr>
                        <th>Website</th>
                        <td>{{ $company->website }}</td>
                    </tr>
                    <tr>
                        <th>Address</th>
                        <td>{{ $company->address }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection
